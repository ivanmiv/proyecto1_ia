/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsapiens;

import java.util.ArrayList;

/**
 *
 * @author Jhon Frederick Mena R - 1424886
 * @author Esteban Aguirre Martinez - 1427374
 * @author Ivan Miguel Viveros Rayo - 1427315
 */
public class BusquedaAmplitud extends BusquedaBase {

    public BusquedaAmplitud(int[][] matrizLaberinto, int numero_disparos) {

        super(matrizLaberinto);

        arreglo_nodos = new ArrayList<>();
        nodosExpandidos = 0;
        int fila_inicial = 0, columna_inicial = 0;

        for (int fila = 0; fila < 10; ++fila) {
            for (int columna = 0; columna < 10; ++columna) {
                if (matriz_mundo[fila][columna] == 2) {

                    fila_inicial = fila;
                    columna_inicial = columna;
                    break;
                }
            }
        }

        Nodo nodo_inicial = new Nodo(null, fila_inicial, columna_inicial, numero_disparos, 0, 0);

        arreglo_nodos.add(nodo_inicial);

    }

    /**
     * Expande los nodos hasta encontrar la solucion
     *
     * @return ArrayList<int[]>
     */
    public ArrayList<int[]> expandir() {

        Nodo nodo_actual = arreglo_nodos.get(0);
        arreglo_nodos.remove(0);
        long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecución
        TInicio = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
        int valor_posicion = matriz_mundo[nodo_actual.posicion_fila][nodo_actual.posicion_columna];
        while (!nodo_actual.meta(valor_posicion)) {

            //Movimiento arriba
            moverse(nodo_actual, Nodo.ARRIBA, matriz_mundo, arreglo_nodos, true);

            //Movimiento derecha
            moverse(nodo_actual, Nodo.DERECHA, matriz_mundo, arreglo_nodos, true);

            //Movimiento abajo
            moverse(nodo_actual, Nodo.ABAJO, matriz_mundo, arreglo_nodos, true);

            //Movimiento izquierda
            moverse(nodo_actual, Nodo.IZQUIERDA, matriz_mundo, arreglo_nodos, true);

            if (profundidadTotal < nodo_actual.profundidad) {
                profundidadTotal = nodo_actual.profundidad;
            }

            nodo_actual = arreglo_nodos.get(0);

            valor_posicion = matriz_mundo[nodo_actual.posicion_fila][nodo_actual.posicion_columna];
            arreglo_nodos.remove(0);
            nodosExpandidos += 1;

        }
        if (profundidadTotal < nodo_actual.profundidad) {
                profundidadTotal = nodo_actual.profundidad;
        }
        nodosExpandidos += 1;
        TFin = System.currentTimeMillis(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
        tiempo = TFin - TInicio; //Calculamos los milisegundos de diferencia
        tiempoEjecucion = tiempo;
        hallarProfundidadArbol();
        return nodo_actual.armarCamino();

    }

    /**
     * Aplicara los diferentes operadores que puede realizar el nodos siempre y
     * cuando estos sean permitidos.
     *
     * @param nodo_actual
     * @param operador
     * @param matriz_mundo
     * @param arreglo_nodos
     * @param es_cola
     */
    public void moverse(Nodo nodo_actual, int operador, int[][] matriz_mundo, ArrayList<Nodo> arreglo_nodos, boolean es_cola) {
        int posicion_fila = nodo_actual.posicion_fila;
        int posicion_columna = nodo_actual.posicion_columna;

        if (operador == Nodo.ARRIBA) {
            if (posicion_fila > 0 && matriz_mundo[nodo_actual.posicion_fila - 1][nodo_actual.posicion_columna] != 1) {
                nodo_actual.crearHijo(-1, 0, Nodo.ARRIBA, arreglo_nodos);
            }
        } else if (operador == Nodo.DERECHA) {
            if (posicion_columna < 9 && matriz_mundo[posicion_fila][posicion_columna + 1] != 1) {
                nodo_actual.crearHijo(0, 1, Nodo.DERECHA, arreglo_nodos);
            }
        } else if (operador == Nodo.ABAJO) {
            if (posicion_fila < 9 && matriz_mundo[posicion_fila + 1][posicion_columna] != 1) {
                nodo_actual.crearHijo(1, 0, Nodo.ABAJO, arreglo_nodos);
            }
        } else if (operador == Nodo.IZQUIERDA) {
            if (posicion_columna > 0 && matriz_mundo[posicion_fila][posicion_columna - 1] != 1) {
                nodo_actual.crearHijo(0, -1, Nodo.IZQUIERDA, arreglo_nodos);
            }
        }
    }

}
