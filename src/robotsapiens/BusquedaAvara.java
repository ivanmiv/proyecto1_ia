/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsapiens;

import static java.lang.Math.abs;
import java.util.ArrayList;

/**
 *
 * @author Jhon Frederick Mena R - 1424886
 * @author Esteban Aguirre Martinez - 1427374
 * @author Ivan Miguel Viveros Rayo - 1427315
 */
public class BusquedaAvara extends BusquedaBase {

    int fila_item, columna_item;

    public BusquedaAvara(int[][] matrizLaberinto, int cantidad_Disparos) {
        super(matrizLaberinto);
        nodosExpandidos = 0;
        arreglo_nodos = new ArrayList<>();

        int fila_inicial = 0, columna_inicial = 0;

        for (int fila = 0; fila < 10; ++fila) {
            for (int columna = 0; columna < 10; ++columna) {
                if (matriz_mundo[fila][columna] == 2) {

                    fila_inicial = fila;
                    columna_inicial = columna;
                }
                if (matriz_mundo[fila][columna] == 4) {

                    fila_item = fila;
                    columna_item = columna;
                }
            }
        }

        Nodo nodo_inicial = new Nodo(null, fila_inicial, columna_inicial, cantidad_Disparos, 0, 0, 0);

        arreglo_nodos.add(nodo_inicial);
    }

    /**
     * Expande los nodos hasta encontrar la solucion
     *
     * @return ArrayList<int[]>
     */
    public ArrayList<int[]> expandir() {

        Nodo nodo_actual = arreglo_nodos.get(0);
        arreglo_nodos.remove(0);
        long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecución
        TInicio = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
        int valor_posicion = matriz_mundo[nodo_actual.posicion_fila][nodo_actual.posicion_columna];
        while (!nodo_actual.meta(valor_posicion)) {

            //Movimiento arriba
            moverse(nodo_actual, Nodo.ARRIBA, matriz_mundo, arreglo_nodos, true);

            //Movimiento derecha
            moverse(nodo_actual, Nodo.DERECHA, matriz_mundo, arreglo_nodos, true);

            //Movimiento abajo
            moverse(nodo_actual, Nodo.ABAJO, matriz_mundo, arreglo_nodos, true);

            //Movimiento izquierda
            moverse(nodo_actual, Nodo.IZQUIERDA, matriz_mundo, arreglo_nodos, true);

            if (profundidadTotal < nodo_actual.profundidad) {
                profundidadTotal = nodo_actual.profundidad;
            }
            nodo_actual = heuristicaMinima(arreglo_nodos);

            valor_posicion = matriz_mundo[nodo_actual.posicion_fila][nodo_actual.posicion_columna];
            arreglo_nodos.remove(nodo_actual);
            nodosExpandidos += 1;

            //System.out.println("cantidad nodos en cola "+arreglo_nodos.size()+ "busqueda "+nodo_actual.posicion_fila+" "+nodo_actual.posicion_columna);
        }
        if (profundidadTotal < nodo_actual.profundidad) {
            profundidadTotal = nodo_actual.profundidad;
        }
        nodosExpandidos += 1;
        TFin = System.currentTimeMillis(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
        tiempo = TFin - TInicio; //Calculamos los milisegundos de diferencia
        tiempoEjecucion = tiempo;
        hallarProfundidadArbol();
        return nodo_actual.armarCamino();

    }

    /**
     * Aplicara los diferentes operadores que puede realizar el nodos siempre y
     * cuando estos sean permitidos.
     *
     * @param nodo_actual
     * @param operador
     * @param matriz_mundo
     * @param arreglo_nodos
     * @param es_cola
     */
    public void moverse(Nodo nodo_actual, int operador, int[][] matriz_mundo, ArrayList<Nodo> arreglo_nodos, boolean es_cola) {
        int posicion_fila = nodo_actual.posicion_fila;
        int posicion_columna = nodo_actual.posicion_columna;

        if (operador == Nodo.ARRIBA) {
            if (posicion_fila > 0 && matriz_mundo[posicion_fila - 1][posicion_columna] != 1) {

                nodo_actual.crearHijo(-1, 0, 0, 0, heuristicaDistanciaManhattan(posicion_fila - 1, posicion_columna), Nodo.ARRIBA, arreglo_nodos, es_cola);

            }
        } else if (operador == Nodo.DERECHA) {
            if (posicion_columna < 9 && matriz_mundo[posicion_fila][posicion_columna + 1] != 1) {

                nodo_actual.crearHijo(0, 1, 0, 0, heuristicaDistanciaManhattan(posicion_fila, posicion_columna + 1), Nodo.DERECHA, arreglo_nodos, es_cola);

            }
        } else if (operador == Nodo.ABAJO) {
            if (posicion_fila < 9 && matriz_mundo[posicion_fila + 1][posicion_columna] != 1) {

                nodo_actual.crearHijo(1, 0, 0, 0, heuristicaDistanciaManhattan(posicion_fila + 1, posicion_columna), Nodo.ABAJO, arreglo_nodos, es_cola);

            }
        } else if (operador == Nodo.IZQUIERDA) {
            if (posicion_columna > 0 && matriz_mundo[posicion_fila][posicion_columna - 1] != 1) {

                nodo_actual.crearHijo(0, -1, 0, 0, heuristicaDistanciaManhattan(posicion_fila, posicion_columna - 1), Nodo.IZQUIERDA, arreglo_nodos, es_cola);

            }
        }
    }

    /**
     * Retorna el nodo que posea el costo minimo dado por la heuristica
     *
     * @param arreglo_nodos
     * @return Nodo
     */
    private Nodo heuristicaMinima(ArrayList<Nodo> arreglo_nodos) {
        int pos_nodo = 0;
        int menor_heuristica = arreglo_nodos.get(0).valor_heuristica;
        for (int i = 0; i < arreglo_nodos.size(); i++) {
            int heuristica_i = arreglo_nodos.get(i).valor_heuristica;
            if (heuristica_i < menor_heuristica) {
                pos_nodo = i;
                menor_heuristica = heuristica_i;
            }
        }

        return arreglo_nodos.get(pos_nodo);
    }

    /**
     * Determina la distancia que hay desde un punto hasta la meta
     *
     * @param fila
     * @param columna
     * @return int
     */
    private int heuristicaDistanciaManhattan(int fila, int columna) {
        return abs(fila - fila_item) + abs(columna - columna_item);
    }

}
