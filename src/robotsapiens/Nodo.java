/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsapiens;

import java.util.ArrayList;

/**
 *
 * @author Jhon Frederick Mena R - 1424886
 * @author Esteban Aguirre Martinez - 1427374
 * @author Ivan Miguel Viveros Rayo - 1427315
 */
public class Nodo {

    final private Nodo nodoPadre;
    int cantidad_disparos;
    int costo;
    int posicion_fila;
    int posicion_columna;
    int profundidad;
    int operador;
    int valor_heuristica;
    ArrayList<int[]> camino = new ArrayList<>();
    static int ARRIBA = 1;
    static int DERECHA = 2;
    static int ABAJO = 3;
    static int IZQUIERDA = 4;

    /**
     * Constructor de la clase nodo sin costo
     *
     * @param nodoPadre
     * @param cantidad_disparos
     * @param posicion_fila
     * @param posicion_columna
     * @param profundidad
     * @param operador
     */
    public Nodo(Nodo nodoPadre, int posicion_fila, int posicion_columna, int cantidad_disparos, int operador, int profundidad) {
        this.nodoPadre = nodoPadre;
        this.cantidad_disparos = cantidad_disparos;
        this.posicion_fila = posicion_fila;
        this.posicion_columna = posicion_columna;
        this.profundidad = profundidad;
        this.operador = operador;
    }

    /**
     * Constructor de la clase nodo con costo
     *
     * @param nodoPadre
     * @param cantidad_disparos
     * @param costo
     * @param posicion_fila
     * @param posicion_columna
     * @param profundidad
     * @param operador
     */
    public Nodo(Nodo nodoPadre, int posicion_fila, int posicion_columna, int cantidad_disparos, int costo, int operador, int profundidad) {
        this.nodoPadre = nodoPadre;
        this.cantidad_disparos = cantidad_disparos;
        this.costo = costo;
        this.posicion_fila = posicion_fila;
        this.posicion_columna = posicion_columna;
        this.profundidad = profundidad;
        this.operador = operador;
    }

    /**
     * Constructor de la clase nodo con costo y heuristica
     *
     * @param nodoPadre
     * @param posicion_fila
     * @param posicion_columna
     * @param cantidad_disparos
     * @param costo
     * @param operador
     * @param profundidad
     * @param valor_heuristica
     */
    public Nodo(Nodo nodoPadre, int posicion_fila, int posicion_columna, int cantidad_disparos, int costo, int valor_heuristica, int operador, int profundidad) {
        this.nodoPadre = nodoPadre;
        this.cantidad_disparos = cantidad_disparos;
        this.costo = costo;
        this.posicion_fila = posicion_fila;
        this.posicion_columna = posicion_columna;
        this.profundidad = profundidad;
        this.operador = operador;
        this.valor_heuristica = valor_heuristica;
    }

    
    
    /**
     * Permite verificar si la posicion es la meta
     *
     * @param valor_posicion
     * @return boolean
     */
    public boolean meta(int valor_posicion) {
        return valor_posicion == 4;
    }

        /**
     * Crear hijo a partir del padre sin considearar el costo
     *
     * @param cambio_fila
     * @param cambio_columna
     * @param operador
     * @param arreglo_nodos
     */
    public void crearHijo(int cambio_fila, int cambio_columna, int operador, ArrayList<Nodo> arreglo_nodos) {
        Nodo nuevo_nodo = new Nodo(this,
                posicion_fila + cambio_fila,
                posicion_columna + cambio_columna,
                cantidad_disparos,
                operador,
                profundidad + 1);
        if (validarNodo(nuevo_nodo)) {            
                arreglo_nodos.add(nuevo_nodo);            
        }
    }
    
    
    /**
     * Crear hijo a partir del padre sin considearar el costo
     *
     * @param cambio_fila
     * @param cambio_columna
     * @param operador
     * @param arreglo_nodos
     * @param posicion
     */
    public void crearHijo(int cambio_fila, int cambio_columna, int operador, ArrayList<Nodo> arreglo_nodos, int posicion) {
        Nodo nuevo_nodo = new Nodo(this,
                posicion_fila + cambio_fila,
                posicion_columna + cambio_columna,
                cantidad_disparos,
                operador,
                profundidad + 1);
        if (validarNodo(nuevo_nodo)) {
            
            //En caso de que la posicion en que se quiera insertar sea mayor al tamaño del arreglo restamos 1 hasta que quede menor al tamaño del arreglo (para que se inserte de ultimo)
            while(true){
                if (posicion>arreglo_nodos.size()){
                    posicion-=1;
                }else {
                    break;
                }
            }
               
            arreglo_nodos.add(posicion, nuevo_nodo);
           
        }
    }
    

    /**
     * Crear hijo a partir del padre considerando el costo
     *
     * @param cambio_fila
     * @param cambio_columna
     * @param cambio_disparos
     * @param cambio_costo
     * @param operador
     * @param arreglo_nodos
     * @param es_cola
     */
    public void crearHijo(int cambio_fila, int cambio_columna, int cambio_disparos, int cambio_costo, int operador, ArrayList<Nodo> arreglo_nodos, boolean es_cola) {
        Nodo nuevo_nodo = new Nodo(this,
                posicion_fila + cambio_fila,
                posicion_columna + cambio_columna,
                cantidad_disparos + cambio_disparos,
                costo + cambio_costo,
                operador,
                profundidad + 1);
        if (validarNodo(nuevo_nodo)) {
            if (es_cola) {
                arreglo_nodos.add(nuevo_nodo);
            } else {
                arreglo_nodos.add(0, nuevo_nodo);
            };
        }
    }

    /**
     * Crear hijo a partir del padre considerando costo y heuristica
     *
     * @param cambio_fila
     * @param cambio_columna
     * @param cambio_disparos
     * @param cambio_costo
     * @param valor_heuristica
     * @param operador
     * @param arreglo_nodos
     * @param es_cola
     */
    public void crearHijo(int cambio_fila, int cambio_columna, int cambio_disparos, int cambio_costo, int valor_heuristica, int operador, ArrayList<Nodo> arreglo_nodos, boolean es_cola) {
        Nodo nuevo_nodo = new Nodo(this,
                posicion_fila + cambio_fila,
                posicion_columna + cambio_columna,
                cantidad_disparos + cambio_disparos,
                costo + cambio_costo,
                valor_heuristica,
                operador,
                profundidad + 1);
        if (validarNodo(nuevo_nodo)) {
            if (es_cola) {
                arreglo_nodos.add(nuevo_nodo);
            } else {
                arreglo_nodos.add(0, nuevo_nodo);
            };
        }
    }

    /**
     * Retorna el nodo padre
     * @return Nodo
     */
    public Nodo getNodoPadre() {
        return nodoPadre;
    }

    /**
     * Determina si es valida la creacion del nodo
     * @param nodo
     * @return
     */
    public boolean validarNodo(Nodo nodo) {

        boolean valido = false;
        try {
            int posicion_padre_fila = nodo.getNodoPadre().getNodoPadre().posicion_fila;
            int posicion_padre_columna = nodo.getNodoPadre().getNodoPadre().posicion_columna;
            valido = !((nodo.posicion_columna == posicion_padre_columna)
                    && (nodo.posicion_fila == posicion_padre_fila));
        } catch (NullPointerException e) {
            return true;
        }

        return valido;
    }

    /**
     * Retorna el camino que debe seguir el robot para llegar a la meta, dicho
     * camino son las posiciones que el robot dede tomar
     *
     * @return ArrayList<int[]>
     */
    public ArrayList<int[]> armarCamino() {
        Nodo nodo_padre;
        //Se agrega la posicion inicial desde donde se arranca el camino
        this.camino.add(new int[]{this.posicion_fila, this.posicion_columna});
        nodo_padre = this.nodoPadre;

        while (nodo_padre != null) {
            int posicion[] = new int[2];
            posicion[0] = nodo_padre.posicion_fila;
            posicion[1] = nodo_padre.posicion_columna;

            this.camino.add(0, posicion);
            nodo_padre = nodo_padre.getNodoPadre();
        }
        return this.camino;

    }

}
