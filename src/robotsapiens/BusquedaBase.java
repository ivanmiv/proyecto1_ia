/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsapiens;

import java.util.ArrayList;

/**
 *
 * @author Jhon Frederick Mena R - 1424886
 * @author Esteban Aguirre Martinez - 1427374
 * @author Ivan Miguel Viveros Rayo - 1427315
 */
public class BusquedaBase {

    protected int matriz_mundo[][];
    protected long tiempoEjecucion;
    protected int nodosExpandidos;
    protected int profundidadTotal;
    protected ArrayList<Nodo> arreglo_nodos;
    protected int costoSolucion;

    public BusquedaBase(int[][] matriz_mundo) {
        this.matriz_mundo = matriz_mundo;
    }

    /**
     * Retorna la profundidad el arbol de busqueda
     *
     * @return int
     */
    public int getProfundidadTotal() {
        return profundidadTotal;
    }

    /**
     * Retorna el total de nodos expandidos por el algoritmo de busqueda
     *
     * @return int
     */
    public int getNodosExpandidos() {
        return nodosExpandidos;
    }

    /**
     * Retorna el tiempo de ejecucuion del algoritmo de busqueda
     *
     * @return long
     */
    public long getTiempoEjecucion() {
        return tiempoEjecucion;
    }

    /**
     * Determina cual es la profundidad que se alcanzo en el arbol
     */
    public void hallarProfundidadArbol() {

        for (int indice = 0; indice < arreglo_nodos.size(); indice++) {
            if (profundidadTotal < arreglo_nodos.get(indice).profundidad) {
                profundidadTotal = arreglo_nodos.get(indice).profundidad;
            }
        }
    }

 
}
